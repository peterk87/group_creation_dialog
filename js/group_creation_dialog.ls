prelude = require \prelude-ls

export prelude

update_alert = (cont_el, msg, alert_level='info', prepend=false) !->
  alert_el = jQuery '<div/>', do
    class: "alert alert-dismissable alert-#{alert_level}"
  
  alert_el.html msg

  jQuery '<button 
      type="button" 
      class="close" 
      data-dismiss="alert" 
      aria-hidden="true">
        &times;
      </button>'
    .prependTo alert_el
  

  if prepend
    alert_el.prependTo cont_el
  else
    cont_el.html alert_el


create_control_with_label_jquery_el = (el, label='') ->
  form_group = jQuery '<div/>' class:'form-group'

  if label == ''
    label = el.id
  jQuery '<label/>', {
    for: el.id
    text: label
    }
    .appendTo form_group
  
  el.appendTo form_group

  form_group
  

export class GroupCreationDialog

  group_collections: []
  gc_alert_cont: void
  gc_div_id: void
  _confirm_group_creation: null

  no_group_collections_msg: "
    <p><b>No user group collections created!</b></p>
    <p>Create a user group collection before proceeding.</p>
    "

  existing_group_collections_msg: "
    <p>
      Select or create a new user group collection to save your new group to
    </p>
    "


  (@el, @selection, @group_collections=[], @groups_dict={}) ->
    if not @el?
      throw new Error 'GroupCreationDialog constructor requires non-null element
      for creating GroupCreationDialog object'
    if not el instanceof jQuery
      throw new Error 'GroupCreationDialog constructor requires el argument to
      be a jQuery selection object'
    if not @selection? or @selection.length == 0
      throw new Error 'GroupCreationDialog constructor requires non-zero length
      array of selections'
    if not prelude.all (prelude.is-type 'Boolean'), @selection
      throw new Error 'GroupCreationDialog constructor requires array of Boolean
      type to specify selection list. Not all values are of type Boolean'

    if jQuery.isEmptyObject @groups_dict and @group_collections.length > 0
      for x in @group_collections
        @groups_dict[x] = prelude.replicate @selection.length null

    if not jQuery.isEmptyObject @groups_dict and @group_collections.length == 0
      for x of @groups_dict
        @group_collections.push x

    if prelude.keys @groups_dict .length != @group_collections.length
      throw new Error "GroupCreationDialog constructor: groups_dict dictionary 
      and group_collections list do not have the same number of group 
      collections. Check that your input is correct.
      groups_dict has #{prelude.keys @groups_dict .length} keys.
      group_collections has #{@group_collections.length} items.
      "


    @el_id := @el.attr \id

    @el.attr \title, 'Create group from selection'

    console.log 'ctor GroupCreationDialog', @el, @selection, @group_collections, @groups_dict
    console.log @el_id
    @gc_alert_cont := jQuery \<div/>, {
      class:"gc_alert_cont"
      id:"gc_alert_cont_#{@el_id}"
      }
      .appendTo @el
    @btn_show_create_group_collection := jQuery \<button/>, {
      class: "btn_show_create_group_collection btn"
      id: "btn_show_create_group_collection_#{@el_id}"
      text: "Create new group collection"
      }
      .appendTo @el

    @gc_controls_cont := jQuery \<div/>, {
      class:'gc_controls_cont'
      id:"gc_controls_cont_#{@el_id}"
      }
      .appendTo @el

    @_el_parent := @el.parent!
    @el := @el.dialog do
      autoOpen: false
      width: 400
      buttons: [
        text: \OK
        class: 'btn'
        click: !-> $ @ .dialog \close
      ]
    @el.parent!prependTo @_el_parent

    @_create_gc_coll_dialog!

    @btn_show_create_group_collection.on \click, !~>
      @gc_coll_dialog.dialog \open

    @el.on 'dialogopen', (event, ui) !~> @_init_gc_dialog!
  

  show: !->
    @el.dialog \open

  update_selection: (selection) ->
    if selection.length != @selection.length
      throw new Error "GroupCreationDialog update_selection: new selection does
       not match the length of the current selection"
    @selection := selection

  _create_group_collection: (v, alert_el=@gc_coll_alert_cont) !->
    # v = @group_collection_creation_name.val!
    if v == ''
      msg = "Cannot create group collection name with empty name"
      alert_level = \danger
    else if v in @group_collections
      msg = "Cannot create group collection '#{v}' since it already exists!"
      alert_level = \danger
    else
      msg = "Created new group collection '#{v}'"
      alert_level = \success
      /*
      Add the newly group collection to the group_collections list
      */
      @group_collections.push v
      /*
      On creation of a new group collection create a new array in groups_dict
      with null as the initial values. 
      */
      @groups_dict[v] = prelude.replicate @selection.length, null
      @el.trigger \change
    /*
    Alert the user to what has happened with regards to group collection 
    creation
    */
    update_alert alert_el, msg, alert_level

  _init_gc_dialog: !->
    if @group_collections.length == 0
      update_alert @gc_alert_cont, @no_group_collections_msg, \danger
      @btn_show_create_group_collection.addClass \btn-primary
      @gc_controls_cont.html ''
    else
      update_alert @gc_alert_cont, @existing_group_collections_msg
      @btn_show_create_group_collection.removeClass \btn-primary

      @gc_controls_cont.html ''
      

      @select_gcoll := jQuery '<select/>', {
        id:'select_gcoll'
        name:'select_gcoll'
        class:'select_gcoll'
        placeholder:'Select or add a group collection...'
        title:'Type to search or to create a new group collection'
        }
      select_gcoll_form_group = create_control_with_label_jquery_el @select_gcoll, label='Select or add a group collection'
      select_gcoll_form_group.appendTo @gc_controls_cont
      
      @select_gcoll.tooltip!
      
      selectize_opts = 
        valueField: 'value'
        labelField: 'text'
        searchField: 'text'
        create: true
        createOnBlur: true
        multiple: false
        options: [{value:x, text:x} for x in @group_collections]
        onOptionAdd: (v,d) !~>
          @_create_group_collection v, alert_el=@gc_alert_cont

      @$select_gcoll := @select_gcoll.selectize selectize_opts
      
      @select_gcoll := @$select_gcoll[0].selectize
      
      # DEBUG @select_gcoll
      window.select_gcoll :=  @select_gcoll

      /*
      Set selected group collection to last created
      */
      @select_gcoll.setValue prelude.last @group_collections

      /*
      Create textbox input for new group name
      */
      @input_group_name = jQuery '<input/>', {
        id: 'group_name'
        name: 'group_name'
        class: 'group_name form-control'
        placeholder: 'Group name'
        }

      group_name_form_group = create_control_with_label_jquery_el @input_group_name, 'Group name'
      group_name_form_group.appendTo @gc_controls_cont

      @input_group_name.on \keyup, (e) !~>
        if e.which == 13
          event.preventDefault!
          @_create_new_group!

      @btn_create_new_group := jQuery '<button/>', {
        class:'btn_create_new_group btn btn-primary'
        text:'Create new group'
        }
        .appendTo @gc_controls_cont

      @btn_create_new_group.on \click !~> @_create_new_group!

  _show_group_creation_confirmation_dialog: (msg) !->
    dialog_div = jQuery \<div/>, {
      class: "confirm_group_creation"
      id: "confirm_group_creation_#{@el_id}"
      title: 'Confirm group creation'
      }
      .appendTo @el

    dialog_div.html msg

    dialog_div.dialog do
      modal: true
      closeText: \hide
      closeOnEscape: false
      buttons:[
      * text: \Yes
        class: 'btn btn-success'
        click: !~> 
          @_confirm_group_creation := true
          dialog_div.dialog \close
      * text: \No
        class: 'btn btn-danger'
        click: !~>
          @_confirm_group_creation := false
          dialog_div.dialog \close
      ]
      close: !~>
        if not @_confirm_group_creation?
          @_confirm_group_creation := false
        @_create_new_group!
      
  _create_new_group: !->
    group_name = @input_group_name.val!
    selected_group_collection = @select_gcoll.getValue!
    g_dict_list = @groups_dict[selected_group_collection]
    is_same_group = [g_dict_list[i] == group_name for b, i in @selection when b]
    
    all_true = prelude.all (== true)

    if group_name == ''
      update_alert @gc_alert_cont, 'New group must have a non-empty name!', 'danger'
      return
    else if all_true is_same_group
      return
    else if group_name in g_dict_list 
    and not @_confirm_group_creation?
      /*
      Show dialog to confirm that the user wants to create a group that 
      already exists in the selected group collection
      */
      msg = "<p><b>
      The group '#{group_name}' already exists in '#{selected_group_collection}'!
      </b></p>
      <p>
      Are you sure you want to add the current selection to group '#{group_name}'?
      </p>"
      @_show_group_creation_confirmation_dialog msg
      return
    else if @_is_overwriting_group selected_group_collection, group_name
    and not @_confirm_group_creation?
      /*
      Show dialog to confirm if the user wants to create a group that will
      overwrite the membership of some of the selection in the group
      collection
      */
      msg = """
      <p><b>Overwrite previous groups?</b></p>
      <p>
      Are you sure you want to overwrite the previous group memberships
      of your selection in the group collection '#{selected_group_collection}'
      with the new group '#{group_name}'?
      </p>
      """
      @_show_group_creation_confirmation_dialog msg
      return
    else if not @_confirm_group_creation? or @_confirm_group_creation
      /*
      If the selected group collection does not exist in the @groups_dict then
      init the group collection with a list of null values with the same length
      as @selection
      */
      if selected_group_collection not of @groups_dict
        @groups_dict[selected_group_collection] = prelude.replicate @selection.length, null

      /*
      Create new list for @groups_dict with new group name for current selection
      */
      old_list = @groups_dict[selected_group_collection]
      new_list = []
      for x,i in old_list
        if @selection[i]
          new_list.push group_name
        else
          new_list.push x

      @groups_dict[selected_group_collection] = new_list

      msg = "Added current selection to new group, '#{group_name}', 
      in group collection '#{selected_group_collection}'"
      update_alert @gc_alert_cont, msg, \success
      @el.trigger \change


    @_confirm_group_creation := null

  /*
  Does the current selection overwrite an existing group?
  If there is a group in the selected group collection that is not null (or NA
  in R) and it's part of the current selection, then the group membership will
  be overwritten. 
  The user should be alerted when this is about to happen in case they don't 
  want to overwrite any group memberships.
  */
  _is_overwriting_group: (gcoll, group) ->
    for x,i in @groups_dict[gcoll]
    
      if x? and @selection[i]
        return true
    /*
    Current selection will not overwrite any existing groups
    */
    return false

  /**
   * Create a group collection creation modal dialog
   */
  _create_gc_coll_dialog: !->
    /*
    Create group collection creation dialog container
    */
    @gc_coll_dialog := jQuery \<div/>, {
      class: "gc_coll_dialog" 
      id: "gc_coll_dialog_#{@el_id}"
      title: "Create group collection"
      }
      .appendTo @el
    /*
    Add an alert container to the group collection creation dialog container
    */
    @gc_coll_alert_cont := jQuery \<div/>, {
      class: "gc_coll_alert_cont" 
      id: "gc_coll_alert_cont_#{@el_id}"
      }
      .appendTo @gc_coll_dialog
    /*
    Add an input textbox so the user can specify a name for a new group
    collection
    */
    @group_collection_creation_name := jQuery \<input/>, {
      class:"group_collection_creation_name form-control"
      id:"group_collection_creation_name_#{@el_id}"
      }
    /*
    Create a form group with a label for the group collection creation input 
    textbox and append to the group collection creation container
    */
    form_group_group_collection_creation_name = create_control_with_label_jquery_el @group_collection_creation_name, 'New Group Collection Name:'
    form_group_group_collection_creation_name.appendTo @gc_coll_dialog
    /*
    Add a button that the user can click to create a group collection.
    Also allow the user to create a group collection by pressing the Enter key
    */
    @btn_create_group_collection := jQuery \<button/>, {
      class: "btn_create_group_collection btn"
      id: "btn_create_group_collection_#{@el_id}"
      text: "Create group collection"
      }
      .appendTo @gc_coll_dialog

    @btn_create_group_collection.on \click !~> 
      @_create_group_collection @group_collection_creation_name.val!

    @group_collection_creation_name.on \keyup, (event) !~>
      # Enter key code == 13
      if event.which == 13
        event.preventDefault!
        @_create_group_collection @group_collection_creation_name.val!

    /*
    Create a jQueryUI modal dialog for group collection creation so that it 
    demands user focus.
    */
    @gc_coll_dialog.dialog do
      autoOpen: false
      width: 400
      modal: true
      buttons: [
        text: \OK
        class: 'btn'
        click: !-> $ @ .dialog \close
      ]
    /*
    When the group collection dialog is opened, set the group collection name 
    input to '' and give it focus
    */
    @gc_coll_dialog.on \dialogopen, (event, ui) !~>
      update_alert @gc_coll_alert_cont, 'Create a new group collection'
      @group_collection_creation_name.val ''
      @group_collection_creation_name.focus!

    /*
    When the group collection dialog is closed, re-initialize the group creation
    dialog
    */
    @gc_coll_dialog.on \dialogclose, (event, ui) !~> @_init_gc_dialog!
