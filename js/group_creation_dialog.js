// Generated by LiveScript 1.2.0
(function(){
  var prelude, update_alert, create_control_with_label_jquery_el, GroupCreationDialog, out$ = typeof exports != 'undefined' && exports || this;
  prelude = require('prelude-ls');
  out$.prelude = prelude;
  update_alert = function(cont_el, msg, alert_level, prepend){
    var alert_el;
    alert_level == null && (alert_level = 'info');
    prepend == null && (prepend = false);
    alert_el = jQuery('<div/>', {
      'class': "alert alert-dismissable alert-" + alert_level
    });
    alert_el.html(msg);
    jQuery('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>').prependTo(alert_el);
    if (prepend) {
      alert_el.prependTo(cont_el);
    } else {
      cont_el.html(alert_el);
    }
  };
  create_control_with_label_jquery_el = function(el, label){
    var form_group;
    label == null && (label = '');
    form_group = jQuery('<div/>', {
      'class': 'form-group'
    });
    if (label === '') {
      label = el.id;
    }
    jQuery('<label/>', {
      'for': el.id,
      text: label
    }).appendTo(form_group);
    el.appendTo(form_group);
    return form_group;
  };
  out$.GroupCreationDialog = GroupCreationDialog = (function(){
    GroupCreationDialog.displayName = 'GroupCreationDialog';
    var prototype = GroupCreationDialog.prototype, constructor = GroupCreationDialog;
    prototype.group_collections = [];
    prototype.gc_alert_cont = void 8;
    prototype.gc_div_id = void 8;
    prototype._confirm_group_creation = null;
    prototype.no_group_collections_msg = "<p><b>No user group collections created!</b></p><p>Create a user group collection before proceeding.</p>";
    prototype.existing_group_collections_msg = "<p>Select or create a new user group collection to save your new group to</p>";
    function GroupCreationDialog(el, selection, group_collections, groups_dict){
      var i$, ref$, len$, x, this$ = this;
      this.el = el;
      this.selection = selection;
      this.group_collections = group_collections != null
        ? group_collections
        : [];
      this.groups_dict = groups_dict != null
        ? groups_dict
        : {};
      if (this.el == null) {
        throw new Error('GroupCreationDialog constructor requires non-null elementfor creating GroupCreationDialog object');
      }
      if (!el instanceof jQuery) {
        throw new Error('GroupCreationDialog constructor requires el argument tobe a jQuery selection object');
      }
      if (this.selection == null || this.selection.length === 0) {
        throw new Error('GroupCreationDialog constructor requires non-zero lengtharray of selections');
      }
      if (!prelude.all(prelude.isType('Boolean'), this.selection)) {
        throw new Error('GroupCreationDialog constructor requires array of Booleantype to specify selection list. Not all values are of type Boolean');
      }
      if (jQuery.isEmptyObject(this.groups_dict) && this.group_collections.length > 0) {
        for (i$ = 0, len$ = (ref$ = this.group_collections).length; i$ < len$; ++i$) {
          x = ref$[i$];
          this.groups_dict[x] = prelude.replicate(this.selection.length(null));
        }
      }
      if (!jQuery.isEmptyObject(this.groups_dict) && this.group_collections.length === 0) {
        for (x in this.groups_dict) {
          this.group_collections.push(x);
        }
      }
      if (prelude.keys(this.groups_dict).length !== this.group_collections.length) {
        throw new Error("GroupCreationDialog constructor: groups_dict dictionary and group_collections list do not have the same number of group collections. Check that your input is correct.groups_dict has " + prelude.keys(this.groups_dict).length + " keys.group_collections has " + this.group_collections.length + " items.");
      }
      this.el_id = this.el.attr('id');
      this.el.attr('title', 'Create group from selection');
      console.log('ctor GroupCreationDialog', this.el, this.selection, this.group_collections, this.groups_dict);
      console.log(this.el_id);
      this.gc_alert_cont = jQuery('<div/>', {
        'class': "gc_alert_cont",
        id: "gc_alert_cont_" + this.el_id
      }).appendTo(this.el);
      this.btn_show_create_group_collection = jQuery('<button/>', {
        'class': "btn_show_create_group_collection btn",
        id: "btn_show_create_group_collection_" + this.el_id,
        text: "Create new group collection"
      }).appendTo(this.el);
      this.gc_controls_cont = jQuery('<div/>', {
        'class': 'gc_controls_cont',
        id: "gc_controls_cont_" + this.el_id
      }).appendTo(this.el);
      this._el_parent = this.el.parent();
      this.el = this.el.dialog({
        autoOpen: false,
        width: 400,
        buttons: [{
          text: 'OK',
          'class': 'btn',
          click: function(){
            $(this).dialog('close');
          }
        }]
      });
      this.el.parent().prependTo(this._el_parent);
      this._create_gc_coll_dialog();
      this.btn_show_create_group_collection.on('click', function(){
        this$.gc_coll_dialog.dialog('open');
      });
      this.el.on('dialogopen', function(event, ui){
        this$._init_gc_dialog();
      });
    }
    prototype.show = function(){
      this.el.dialog('open');
    };
    prototype.update_selection = function(selection){
      if (selection.length !== this.selection.length) {
        throw new Error("GroupCreationDialog update_selection: new selection doesnot match the length of the current selection");
      }
      return this.selection = selection;
    };
    prototype._create_group_collection = function(v, alert_el){
      var msg, alert_level;
      alert_el == null && (alert_el = this.gc_coll_alert_cont);
      if (v === '') {
        msg = "Cannot create group collection name with empty name";
        alert_level = 'danger';
      } else if (in$(v, this.group_collections)) {
        msg = "Cannot create group collection '" + v + "' since it already exists!";
        alert_level = 'danger';
      } else {
        msg = "Created new group collection '" + v + "'";
        alert_level = 'success';
        /*
        Add the newly group collection to the group_collections list
        */
        this.group_collections.push(v);
        /*
        On creation of a new group collection create a new array in groups_dict
        with null as the initial values. 
        */
        this.groups_dict[v] = prelude.replicate(this.selection.length, null);
        this.el.trigger('change');
      }
      /*
      Alert the user to what has happened with regards to group collection 
      creation
      */
      update_alert(alert_el, msg, alert_level);
    };
    prototype._init_gc_dialog = function(){
      var select_gcoll_form_group, label, selectize_opts, x, group_name_form_group, this$ = this;
      if (this.group_collections.length === 0) {
        update_alert(this.gc_alert_cont, this.no_group_collections_msg, 'danger');
        this.btn_show_create_group_collection.addClass('btn-primary');
        this.gc_controls_cont.html('');
      } else {
        update_alert(this.gc_alert_cont, this.existing_group_collections_msg);
        this.btn_show_create_group_collection.removeClass('btn-primary');
        this.gc_controls_cont.html('');
        this.select_gcoll = jQuery('<select/>', {
          id: 'select_gcoll',
          name: 'select_gcoll',
          'class': 'select_gcoll',
          placeholder: 'Select or add a group collection...',
          title: 'Type to search or to create a new group collection'
        });
        select_gcoll_form_group = create_control_with_label_jquery_el(this.select_gcoll, label = 'Select or add a group collection');
        select_gcoll_form_group.appendTo(this.gc_controls_cont);
        this.select_gcoll.tooltip();
        selectize_opts = {
          valueField: 'value',
          labelField: 'text',
          searchField: 'text',
          create: true,
          createOnBlur: true,
          multiple: false,
          options: (function(){
            var i$, ref$, len$, results$ = [];
            for (i$ = 0, len$ = (ref$ = this.group_collections).length; i$ < len$; ++i$) {
              x = ref$[i$];
              results$.push({
                value: x,
                text: x
              });
            }
            return results$;
          }.call(this)),
          onOptionAdd: function(v, d){
            var alert_el;
            this$._create_group_collection(v, alert_el = this$.gc_alert_cont);
          }
        };
        this.$select_gcoll = this.select_gcoll.selectize(selectize_opts);
        this.select_gcoll = this.$select_gcoll[0].selectize;
        window.select_gcoll = this.select_gcoll;
        /*
        Set selected group collection to last created
        */
        this.select_gcoll.setValue(prelude.last(this.group_collections));
        /*
        Create textbox input for new group name
        */
        this.input_group_name = jQuery('<input/>', {
          id: 'group_name',
          name: 'group_name',
          'class': 'group_name form-control',
          placeholder: 'Group name'
        });
        group_name_form_group = create_control_with_label_jquery_el(this.input_group_name, 'Group name');
        group_name_form_group.appendTo(this.gc_controls_cont);
        this.input_group_name.on('keyup', function(e){
          if (e.which === 13) {
            event.preventDefault();
            this$._create_new_group();
          }
        });
        this.btn_create_new_group = jQuery('<button/>', {
          'class': 'btn_create_new_group btn btn-primary',
          text: 'Create new group'
        }).appendTo(this.gc_controls_cont);
        this.btn_create_new_group.on('click', function(){
          this$._create_new_group();
        });
      }
    };
    prototype._show_group_creation_confirmation_dialog = function(msg){
      var dialog_div, this$ = this;
      dialog_div = jQuery('<div/>', {
        'class': "confirm_group_creation",
        id: "confirm_group_creation_" + this.el_id,
        title: 'Confirm group creation'
      }).appendTo(this.el);
      dialog_div.html(msg);
      dialog_div.dialog({
        modal: true,
        closeText: 'hide',
        closeOnEscape: false,
        buttons: [
          {
            text: 'Yes',
            'class': 'btn btn-success',
            click: function(){
              this$._confirm_group_creation = true;
              dialog_div.dialog('close');
            }
          }, {
            text: 'No',
            'class': 'btn btn-danger',
            click: function(){
              this$._confirm_group_creation = false;
              dialog_div.dialog('close');
            }
          }
        ],
        close: function(){
          if (this$._confirm_group_creation == null) {
            this$._confirm_group_creation = false;
          }
          this$._create_new_group();
        }
      });
    };
    prototype._create_new_group = function(){
      var group_name, selected_group_collection, g_dict_list, is_same_group, res$, i$, ref$, len$, i, b, all_true, msg, old_list, new_list, x;
      group_name = this.input_group_name.val();
      selected_group_collection = this.select_gcoll.getValue();
      g_dict_list = this.groups_dict[selected_group_collection];
      res$ = [];
      for (i$ = 0, len$ = (ref$ = this.selection).length; i$ < len$; ++i$) {
        i = i$;
        b = ref$[i$];
        if (b) {
          res$.push(g_dict_list[i] === group_name);
        }
      }
      is_same_group = res$;
      all_true = prelude.all((function(it){
        return it === true;
      }));
      if (group_name === '') {
        update_alert(this.gc_alert_cont, 'New group must have a non-empty name!', 'danger');
        return;
      } else if (all_true(is_same_group)) {
        return;
      } else if (in$(group_name, g_dict_list) && this._confirm_group_creation == null) {
        /*
        Show dialog to confirm that the user wants to create a group that 
        already exists in the selected group collection
        */
        msg = "<p><b>The group '" + group_name + "' already exists in '" + selected_group_collection + "'!</b></p><p>Are you sure you want to add the current selection to group '" + group_name + "'?</p>";
        this._show_group_creation_confirmation_dialog(msg);
        return;
      } else if (this._is_overwriting_group(selected_group_collection, group_name) && this._confirm_group_creation == null) {
        /*
        Show dialog to confirm if the user wants to create a group that will
        overwrite the membership of some of the selection in the group
        collection
        */
        msg = "<p><b>Overwrite previous groups?</b></p>\n<p>\nAre you sure you want to overwrite the previous group memberships\nof your selection in the group collection '" + selected_group_collection + "'\nwith the new group '" + group_name + "'?\n</p>";
        this._show_group_creation_confirmation_dialog(msg);
        return;
      } else if (this._confirm_group_creation == null || this._confirm_group_creation) {
        /*
        If the selected group collection does not exist in the @groups_dict then
        init the group collection with a list of null values with the same length
        as @selection
        */
        if (!(selected_group_collection in this.groups_dict)) {
          this.groups_dict[selected_group_collection] = prelude.replicate(this.selection.length, null);
        }
        /*
        Create new list for @groups_dict with new group name for current selection
        */
        old_list = this.groups_dict[selected_group_collection];
        new_list = [];
        for (i$ = 0, len$ = old_list.length; i$ < len$; ++i$) {
          i = i$;
          x = old_list[i$];
          if (this.selection[i]) {
            new_list.push(group_name);
          } else {
            new_list.push(x);
          }
        }
        this.groups_dict[selected_group_collection] = new_list;
        msg = "Added current selection to new group, '" + group_name + "', in group collection '" + selected_group_collection + "'";
        update_alert(this.gc_alert_cont, msg, 'success');
        this.el.trigger('change');
      }
      this._confirm_group_creation = null;
    };
    /*
    Does the current selection overwrite an existing group?
    If there is a group in the selected group collection that is not null (or NA
    in R) and it's part of the current selection, then the group membership will
    be overwritten. 
    The user should be alerted when this is about to happen in case they don't 
    want to overwrite any group memberships.
    */
    prototype._is_overwriting_group = function(gcoll, group){
      var i$, ref$, len$, i, x;
      for (i$ = 0, len$ = (ref$ = this.groups_dict[gcoll]).length; i$ < len$; ++i$) {
        i = i$;
        x = ref$[i$];
        if (x != null && this.selection[i]) {
          return true;
        }
      }
      /*
      Current selection will not overwrite any existing groups
      */
      return false;
    };
    /**
     * Create a group collection creation modal dialog
     */
    prototype._create_gc_coll_dialog = function(){
      /*
      Create group collection creation dialog container
      */
      var form_group_group_collection_creation_name, this$ = this;
      this.gc_coll_dialog = jQuery('<div/>', {
        'class': "gc_coll_dialog",
        id: "gc_coll_dialog_" + this.el_id,
        title: "Create group collection"
      }).appendTo(this.el);
      /*
      Add an alert container to the group collection creation dialog container
      */
      this.gc_coll_alert_cont = jQuery('<div/>', {
        'class': "gc_coll_alert_cont",
        id: "gc_coll_alert_cont_" + this.el_id
      }).appendTo(this.gc_coll_dialog);
      /*
      Add an input textbox so the user can specify a name for a new group
      collection
      */
      this.group_collection_creation_name = jQuery('<input/>', {
        'class': "group_collection_creation_name form-control",
        id: "group_collection_creation_name_" + this.el_id
      });
      /*
      Create a form group with a label for the group collection creation input 
      textbox and append to the group collection creation container
      */
      form_group_group_collection_creation_name = create_control_with_label_jquery_el(this.group_collection_creation_name, 'New Group Collection Name:');
      form_group_group_collection_creation_name.appendTo(this.gc_coll_dialog);
      /*
      Add a button that the user can click to create a group collection.
      Also allow the user to create a group collection by pressing the Enter key
      */
      this.btn_create_group_collection = jQuery('<button/>', {
        'class': "btn_create_group_collection btn",
        id: "btn_create_group_collection_" + this.el_id,
        text: "Create group collection"
      }).appendTo(this.gc_coll_dialog);
      this.btn_create_group_collection.on('click', function(){
        this$._create_group_collection(this$.group_collection_creation_name.val());
      });
      this.group_collection_creation_name.on('keyup', function(event){
        if (event.which === 13) {
          event.preventDefault();
          this$._create_group_collection(this$.group_collection_creation_name.val());
        }
      });
      /*
      Create a jQueryUI modal dialog for group collection creation so that it 
      demands user focus.
      */
      this.gc_coll_dialog.dialog({
        autoOpen: false,
        width: 400,
        modal: true,
        buttons: [{
          text: 'OK',
          'class': 'btn',
          click: function(){
            $(this).dialog('close');
          }
        }]
      });
      /*
      When the group collection dialog is opened, set the group collection name 
      input to '' and give it focus
      */
      this.gc_coll_dialog.on('dialogopen', function(event, ui){
        update_alert(this$.gc_coll_alert_cont, 'Create a new group collection');
        this$.group_collection_creation_name.val('');
        this$.group_collection_creation_name.focus();
      });
      /*
      When the group collection dialog is closed, re-initialize the group creation
      dialog
      */
      this.gc_coll_dialog.on('dialogclose', function(event, ui){
        this$._init_gc_dialog();
      });
    };
    return GroupCreationDialog;
  }());
  function in$(x, xs){
    var i = -1, l = xs.length >>> 0;
    while (++i < l) if (x === xs[i]) return true;
    return false;
  }
}).call(this);
