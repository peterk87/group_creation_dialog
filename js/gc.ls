# prelude = require \prelude-ls

# export prelude

$ document .ready ->

  bool_to_binary = (b) -> if b then '1' else '0'

  binary_to_bool = (x) -> if x == '1' then true else false

  get_bool_list_from_binary_string = (binary_str) ->
    [binary_to_bool x for x in binary_str.split '']

  invert_binary = (binary_str) ->
    d = 
      '1':'0'
      '0':'1'
    [d[x] for x in binary_str.split ''].join ''


  el = $ '#gc' 
  window.el := el
  input_selection = $ ".selection"
  input_selection.on \keypress (e) ->
    if e.which in [49, 48, 8, 37, 38, 39, 40]
      console.log 'input_selection keypress ALLOWED KEY PRESSED', e.which
    else if e.which == 13
      e.preventDefault!
      # console.log 'input_selection keypress ENTER pressed!'
      selection = get_bool_list_from_binary_string input_selection.val!
      gcd.update_selection selection
    else
      # console.log 'input_selection keypress BAD KEY PRESSED', e.which, e
      e.stopPropagation!

  selection = [true false true true false]
  selection_str_list = []
  for x in selection
    if x
      selection_str_list.push '1'
    else
      selection_str_list.push '0'
  input_selection.val selection_str_list.join ''
  gcd = new GroupCreationDialog el, selection
  window.gcd := gcd

  btn = $ ".btn_create_group"
  btn.on \click, !->
    gcd.show!

  output = $ \.output
  gcd.el.on \change !->
    group_collections = gcd.group_collections

    groups_dict = gcd.groups_dict

    groups_dict_list = []
    for k,v of groups_dict
      v = ["#x" for x in v]
      groups_dict_list.push "<p><b>#k</b> :: #v</p>"

    groups_dict_str = groups_dict_list.join '\n'

    output.html "<p><b>Group collections</b></p>
    #{groups_dict_str}
    "

  btn_invert_binary = $ \.invert_binary
    .on \click !->
      v = input_selection.val!
      v = invert_binary v
      input_selection.val v
      selection = get_bool_list_from_binary_string v
      gcd.update_selection selection
